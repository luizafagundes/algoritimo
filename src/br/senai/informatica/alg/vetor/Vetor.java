package br.senai.informatica.alg.vetor;

public class Vetor {

	// criando um vetor para armazenar os dados
	private Cliente[] dados = new Cliente[10];

	// armazena a quantidade de elementos no vetor
	private int tamanho = 0;

	/**
	 * adiciona um elemento no vetor. Se este ultrapassar a capacidade, dobra o
	 * tamanho do vetor de armazenagem
	 * 
	 * @param valor
	 *            - o elemento que ser� adicionado
	 */

	public void adicionar(Cliente valor) {
		// garantindo o tamanho da aloca��o
		this.garantirEspaco();

		// aloca o novo elemento no vetor
		this.dados[this.tamanho] = valor;

		// aumemta a quantidade de elementos adicionados
		this.tamanho++;
	}

	/**
	 * pega do vetor de um dado elemento informando sua posi��o
	 * 
	 * @param posicao
	 *            - a posi��o do elemento
	 * @return - o objeto alocado no vetor
	 */

	public Cliente pegar(int posicao) {
		// valida a posi��o
		this.checarPosicao(posicao);

		return this.dados[posicao];
	}

	/**
	 * devolve a quantidade de elementos alocados no vetor
	 * 
	 * @return
	 */

	public int tamanho() {
		return this.tamanho;
	}

	/**
	 * devolve uma flag indicando se um dado objeto existe dentro do vetor
	 * 
	 * @param valor
	 *            - o objeto procurado
	 * @return
	 */

	public boolean contem(Cliente valor) {
		// percorre cada elemento do vetor e verificar
		// se � igual ao inserido
		for (int i = 0; i < this.tamanho; i++) {
			if (this.dados[i].equals(valor)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * checa se a posi��o � v�lida no vetor. Caso n�o seja dispara uma Exception
	 * 
	 * @param posicao
	 *            - A posicao verificada
	 */

	private void checarPosicao(int posicao) {
		if (posicao < 0 || posicao >= this.tamanho) {
			throw new RuntimeException("A posi��o" + posicao + "� inv�lida");
		}
	}

	/**
	 * verifica se o tamanho do vetor +1 ultrapassa o limite do vetor de aloca��o.
	 * Caso verdadeiro: cria um vetor novo com o dobro de tamanho e passa os
	 * elementos do vetor antigo para o novo
	 */

	private void garantirEspaco() {
		// dobramos o tamanho do vetor se o tamanho l�gico
		// for igual ao fisico
		if (this.dados.length == this.tamanho) {

			// Criando um vetor com o dobro de capacidade
			Cliente[] novoVetor = new Cliente[this.tamanho * 2];

			for (int i = 0; i < this.tamanho; i++) {
				novoVetor[i] = this.dados[i];
			}
			// Determino que o novo vetor ser� o vetor de dados
			this.dados = novoVetor;
		}
	}

	public void remover(int posicao) {
		this.checarPosicao(posicao);

		// remover o elemento da posi��o
		this.dados[posicao] = null;

		// realoca��o para a esquerda
		for (int i = posicao; i < this.tamanho; i++) {
			this.dados[i] = this.dados[i + 1];
			this.dados[i + 1] = null;
		}

		// realoca��o
		this.tamanho--;
	}

	public void adiciona(int posicao, Cliente valor) {
		tamanho++;
		checarPosicao(posicao);
		if (posicao == tamanho) {
			adicionar(valor);
		} else {
			for (int i = tamanho; i >= posicao; i--) {
				garantirEspaco();
				dados[i+1] = dados[i];
			}
			dados[posicao] = valor;
	}

}
}
