package br.senai.informatica.alg.vetor;

public class Cliente {

	public Cliente(String nome) {
		this.setNome(nome);
	}
	/*
	 * Propriedades
	 */

	private String nome;

	/*
	 * Getters & Setters
	 */
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Cliente " + nome + "";
	}

}
