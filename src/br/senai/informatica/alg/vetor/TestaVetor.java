package br.senai.informatica.alg.vetor;

public class TestaVetor {

	public static void main(String[] args) {
	Cliente c1 = new Cliente("Adalberto");
	Cliente c2 = new Cliente("Bernadinho");
	Cliente c3 = new Cliente("Dioguitos");
	Cliente c4 = new Cliente("Felipe");
	Cliente c5 = new Cliente ("Luiza");
	Vetor vetorDeClientes = new Vetor();
	vetorDeClientes.adicionar(c1);
	vetorDeClientes.adicionar(c2);
	vetorDeClientes.adicionar(c3);
	vetorDeClientes.remover(0);
	
	
	System.out.println(vetorDeClientes.pegar(0));
	System.out.println(vetorDeClientes.pegar(1));
	//System.out.println(vetorDeClientes.pegar(2));
	
	//testando o contem
	System.out.println(vetorDeClientes.contem(c2));
	System.out.println(vetorDeClientes.contem(c3));
	
	//tamanho do vetor
	System.out.println(vetorDeClientes.tamanho());
	}

}
